import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  constructor(private http: HttpClient) { }

  getStudentData(): Observable<any> {
    return this.http.get<any>('/assets/mock/student.json')
  }

  getEmployeeData(): Observable<any> {
    return this.http.get<any>('/assets/mock/employee.json')
  }

  getInboundConnector(): Observable<any> {
    return this.http.get<any>('/assets/mock/connectors.json');
  }
}
