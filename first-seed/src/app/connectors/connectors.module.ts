import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConnectorsComponent } from './connectors.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [ConnectorsComponent],
  declarations: [ConnectorsComponent]
})
export class ConnectorsModule {}
