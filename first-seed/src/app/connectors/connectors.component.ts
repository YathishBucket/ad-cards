import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../app-service.service';
import {FilterData} from './filterData'
@Component({
  selector: 'app-connectors',
  templateUrl: './connectors.component.html',
  styleUrls: ['./connectors.component.css']
 
})
export class ConnectorsComponent implements OnInit {
  masterResult:any[];
  result: any[];
  page: number = 1;

  constructor(private _service: AppServiceService) {
    console.log(FilterData);
  }
  ngOnInit() {
    this._service.getInboundConnector().subscribe((data: any) => {
      this.masterResult = data.list;
      this.result=this.masterResult.filter(element=>{
        return FilterData.show.find(x=>  x==element.assetId)
      })
      console.log(this.result);
    })
  }
}
