import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { AppServiceService } from '../app-service.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  result: any[];

  constructor(private _service: AppServiceService) {

  }
  ngOnInit() {
    this._service.getStudentData().subscribe((data: any) => {
      this.result = data;
      this._service.getEmployeeData().subscribe((val: any) => {
        val.map(elm => this.result.push(elm));

      })
      console.log(this.result);
    })
  }


}
