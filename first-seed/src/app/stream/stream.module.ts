import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataComponent } from './data.component';
import { AppServiceService } from '../app-service.service';
import { DataService } from './data.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [DataComponent],
  declarations: [DataComponent],
  providers: [DataService],
})
export class StreamModule { }
