import { NgModule,Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { UrlParserComponent } from './UrlParserComponent.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [UrlParserComponent],
  declarations: [UrlParserComponent]
})
export class UrlParserModule {}
