import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../app-service.service';

@Component({
  selector: 'app-connectors',
  templateUrl: './UrlParserComponent.component.html',
  styleUrls: ['./UrlParserComponent.component.css']
 
})
export class UrlParserComponent implements OnInit {
  urlParsed:boolean;
  url:string='';
  queryString:any;

  constructor(private _service: AppServiceService) {
  
  }
  ngOnInit() {
    this._service.getInboundConnector().subscribe((data: any) => {
      
    })
   
  }
  ParseUrl(){
  console.log("Parseing Url ......");   
  this.urlParsed=true;
  
var uri = this.url;//'http://your.domain/product.aspx?category=4&product_id=2140&query=lcd+tv';
var queryString = {};
uri.replace(
    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
    function($0, $1, $2, $3) { queryString[$1] = $3; }
);
this.queryString=queryString;
console.log('ID: ' + this.queryString['product_id']);     // ID: 2140
console.log('Name: ' + this.queryString['product_name']); // Name: undefined
console.log('Category: ' + this.queryString['category']); // Category: 4
console.log(this.queryString);
  }
  
}
