import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataComponent } from './stream/data.component';
import {UrlParserComponent} from './url/UrlParserComponent.component';
import { ConnectorsComponent } from './connectors/connectors.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: 'home', component: AppComponent },
  { path: 'data', component: DataComponent },
  { path: 'connectors', component: ConnectorsComponent },
  { path: 'url', component: UrlParserComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
