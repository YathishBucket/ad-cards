import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppServiceService } from './app-service.service';
import { HttpClientModule } from '@angular/common/http';
import { StreamModule } from './stream/stream.module';
import { ConnectorsModule } from './connectors/connectors.module';
import { UrlParserModule} from './url/UrlParserComponent.component.module';
import { AppRoutingModule } from './/app-routing.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StreamModule,
    AppRoutingModule,
    ConnectorsModule,
    UrlParserModule
  ],
  providers: [AppServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }



